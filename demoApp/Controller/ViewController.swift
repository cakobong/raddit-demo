//
//  ViewController.swift
//  demoApp
//
//  Created by Akobong Ngonjoh on 9/17/21.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(RadditCell.self, forCellReuseIdentifier: RadditCell.identifier)
        return tableView
    }()
    
    private var postModel: PostModel!
    private var dataSource: postDataSource<RadditCell,dataBranch>!

    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        updateUI()
    }
    
    private func configTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
        
        tableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    func updateUI() {
        self.postModel = PostModel()
        self.postModel.populatePostInfo = {
            self.updatePost()
        }
    }
    
    func updatePost() {
        self.dataSource = postDataSource(cellIdentifier: RadditCell.identifier, allPosts: self.postModel.pstData.data.children, configCell: {(cell, evm) in
            cell.title.text = evm.data.title
            cell.score.text = evm.data.score.justNumbers().description
            cell.commentNumber.text = evm.data.num_comments.justNumbers().description
            let time = Date(timeIntervalSince1970: evm.data.created)
            cell.posterDetails.text = "\(evm.data.subreddit_name_prefixed) • u/\(evm.data.author_fullname) • \(time.timeAgoDisplay())"
            //let liked = evm.data.likes true : false
            cell.likeImageView.image = ImageFactory.shared.likedStatus(false)
            cell.dotsImageView.image = ImageFactory.shared.dot
            cell.likeCountLabel.text = "1"
            if let imageUrl = evm.data.preview?.img.first?.source.url {
                let imageUrl = URL(string: imageUrl)
                cell.postImage.sd_setImage(with: imageUrl, completed: nil)
            }
            cell.upImageView.image = ImageFactory.shared.up
            cell.downImageView.image = ImageFactory.shared.down
            cell.replyImageView.image = ImageFactory.shared.comments
            cell.shareImageView.image = ImageFactory.shared.share
            cell.shareLabel.text = "Share"
        })
        
        DispatchQueue.main.async {
            self.tableView.dataSource = self.dataSource
            self.tableView.delegate = self
            self.tableView.reloadData()
        }
    }


}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 380
    }
}

