//
//  RadditCell.swift
//  demoApp
//
//  Created by Akobong Ngonjoh on 9/20/21.
//

import UIKit
import SDWebImage

class RadditCell: UITableViewCell {
    
    static let identifier = "postCell"
    
    let posterDetails: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    let dotsImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.clipsToBounds = true
        return img
    }()
    let title: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let postImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.clipsToBounds = true
        return img
    }()
    
    let likeImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.clipsToBounds = true
        return img
    }()
    
    let likeCountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let upImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.clipsToBounds = true
        return img
    }()
    
    let score: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let downImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.clipsToBounds = true
        return img
    }()
    
    let replyImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.clipsToBounds = true
        return img
    }()
    
    let commentNumber: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let shareImageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.clipsToBounds = true
        return img
    }()
    
    let shareLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    let likeView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.clipsToBounds = true
      return view
    }()
    
    let topContView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.clipsToBounds = true
      return view
    }()
    
    let statusView: UIView = {
      let view = UIView()
      view.translatesAutoresizingMaskIntoConstraints = false
      view.clipsToBounds = true
      return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
        topContView.addSubview(posterDetails)
        topContView.addSubview(dotsImageView)
        self.contentView.addSubview(topContView)
        self.contentView.addSubview(title)
        self.contentView.addSubview(postImage)
        likeView.addSubview(likeImageView)
        likeView.addSubview(likeCountLabel)
        self.contentView.addSubview(likeView)
        statusView.addSubview(upImageView)
        statusView.addSubview(score)
        statusView.addSubview(downImageView)
        statusView.addSubview(replyImageView)
        statusView.addSubview(commentNumber)
        statusView.addSubview(shareImageView)
        statusView.addSubview(shareLabel)
        self.contentView.addSubview(statusView)
        
        //top view
        topContView.topAnchor.constraint(equalTo:self.contentView.topAnchor, constant: 5).isActive = true
        topContView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        topContView.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant: -10).isActive = true
        topContView.heightAnchor.constraint(equalToConstant:20).isActive = true
        topContView.bottomAnchor.constraint(equalTo: self.title.topAnchor, constant: 5).isActive = true
        
        posterDetails.topAnchor.constraint(equalTo:self.topContView.topAnchor).isActive = true
        posterDetails.leadingAnchor.constraint(equalTo:self.topContView.leadingAnchor).isActive = true
        posterDetails.trailingAnchor.constraint(equalTo:self.topContView.trailingAnchor, constant: 35).isActive = true
        posterDetails.bottomAnchor.constraint(equalTo: self.topContView.bottomAnchor).isActive = true
        
        dotsImageView.topAnchor.constraint(equalTo:self.topContView.topAnchor).isActive = true
        dotsImageView.trailingAnchor.constraint(equalTo:self.topContView.trailingAnchor).isActive = true
        dotsImageView.widthAnchor.constraint(equalToConstant:30).isActive = true
        dotsImageView.heightAnchor.constraint(equalToConstant:30).isActive = true
        dotsImageView.bottomAnchor.constraint(equalTo: self.topContView.bottomAnchor).isActive = true
        
        // Short description
        title.topAnchor.constraint(equalTo:self.topContView.bottomAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        title.widthAnchor.constraint(equalToConstant:self.contentView.frame.size.width - 20).isActive = true
        title.heightAnchor.constraint(equalToConstant:60).isActive = true
        title.bottomAnchor.constraint(equalTo: self.postImage.topAnchor, constant: 5).isActive = true
        
        // post image
        postImage.topAnchor.constraint(equalTo:self.title.bottomAnchor).isActive = true
        postImage.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:0).isActive = true
        postImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0).isActive = true
        postImage.widthAnchor.constraint(equalToConstant:self.contentView.frame.size.width).isActive = true
        postImage.heightAnchor.constraint(equalToConstant:200).isActive = true
        postImage.bottomAnchor.constraint(equalTo: self.likeView.topAnchor, constant: -5).isActive = true
        
        // Like view config
        likeView.topAnchor.constraint(equalTo:self.postImage.bottomAnchor).isActive = true
        likeView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        likeView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10).isActive = true
        likeView.heightAnchor.constraint(equalToConstant:40).isActive = true
        likeView.bottomAnchor.constraint(equalTo: self.statusView.topAnchor, constant: -5).isActive = true
        
        likeImageView.widthAnchor.constraint(equalToConstant:30).isActive = true
        likeImageView.heightAnchor.constraint(equalToConstant:30).isActive = true
        likeImageView.centerYAnchor.constraint(equalTo: self.likeView.centerYAnchor).isActive = true
        likeImageView.leadingAnchor.constraint(equalTo:self.likeView.leadingAnchor, constant:0).isActive = true
        likeImageView.trailingAnchor.constraint(equalTo: self.likeCountLabel.leadingAnchor, constant: -7).isActive = true
        
        likeCountLabel.centerYAnchor.constraint(equalTo: self.likeView.centerYAnchor).isActive = true
        
        // Status view config
        statusView.topAnchor.constraint(equalTo:self.likeView.bottomAnchor).isActive = true
        statusView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        statusView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10).isActive = true
        statusView.widthAnchor.constraint(equalToConstant:self.contentView.frame.size.width - 20).isActive = true
        statusView.heightAnchor.constraint(equalToConstant:50).isActive = true
        
        upImageView.centerYAnchor.constraint(equalTo: self.statusView.centerYAnchor).isActive = true
        upImageView.leadingAnchor.constraint(equalTo: self.statusView.leadingAnchor, constant: 0).isActive = true
        upImageView.trailingAnchor.constraint(equalTo: self.score.leadingAnchor, constant: -5).isActive = true
        upImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        upImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true

        score.centerYAnchor.constraint(equalTo: self.statusView.centerYAnchor).isActive = true
        score.trailingAnchor.constraint(equalTo: self.downImageView.leadingAnchor, constant: -5).isActive = true

        downImageView.centerYAnchor.constraint(equalTo: self.statusView.centerYAnchor).isActive = true
        downImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        downImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true

        
        replyImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        replyImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        replyImageView.centerXAnchor.constraint(equalTo: self.statusView.centerXAnchor, constant: -20).isActive = true
        replyImageView.centerYAnchor.constraint(equalTo: self.statusView.centerYAnchor).isActive = true

        commentNumber.centerXAnchor.constraint(equalTo: self.statusView.centerXAnchor, constant: 20).isActive = true
        commentNumber.centerYAnchor.constraint(equalTo: self.statusView.centerYAnchor).isActive = true

        shareImageView.centerYAnchor.constraint(equalTo: self.statusView.centerYAnchor).isActive = true
        shareImageView.trailingAnchor.constraint(equalTo: self.shareLabel.leadingAnchor, constant: -5).isActive = true
        shareImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        shareImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true

        shareLabel.centerYAnchor.constraint(equalTo: self.statusView.centerYAnchor).isActive = true
        shareLabel.trailingAnchor.constraint(equalTo: self.statusView.trailingAnchor, constant: 0).isActive = true
     }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
}
