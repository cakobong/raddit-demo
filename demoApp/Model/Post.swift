//
//  Post.swift
//  demoApp
//
//  Created by Akobong Ngonjoh on 9/20/21.
//

import Foundation

struct Posts : Decodable {
    let kind: String?
    let data: postData
}

struct postData : Decodable {
    let children: [dataBranch]
}

struct dataBranch : Decodable {
    let data: postDetail
}

struct postDetail: Decodable {
    let title, url, author_fullname, subreddit_name_prefixed: String
    let likes: String?
    let num_comments, score: Int
    let created: Double
    let link_flair_richtext: [descData]
    let preview: preview?
    enum CodingKeys : String, CodingKey {
        case title
        case url
        case num_comments
        case score
        case author_fullname
        case subreddit_name_prefixed
        case created
        case likes
        case link_flair_richtext
        case preview
    }
}

struct descData: Decodable {
    let t: String?
}

struct preview: Decodable {
    let img: [postImages]
    enum CodingKeys : String, CodingKey {
        case img = "images"
    }
}

struct postImages: Decodable {
    let source: source
}

struct source: Decodable {
    let url: String
}
