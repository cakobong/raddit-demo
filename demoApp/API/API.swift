//
//  API.swift
//  demoApp
//
//  Created by Akobong Ngonjoh on 9/20/21.
//

import Foundation

class API : NSObject {
    private let url = URL(string: "https://www.reddit.com/.json")!
    
    func fetchPostData(completion : @escaping (Posts) -> ()) {
        
        URLSession.shared.dataTask(with: url) {(data, urlResponse, error) in
            if let data = data {
                let jsonDecoder = JSONDecoder()
                
                let pstData = try! jsonDecoder.decode(Posts.self, from: data)
                
                completion(pstData)
            }
        }.resume()
    }
}
