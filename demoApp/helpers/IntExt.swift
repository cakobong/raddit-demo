//
//  IntExt.swift
//  demoApp
//
//  Created by Akobong Ngonjoh on 9/27/21.
//

import Foundation

extension Int {
    
    func justNumbers() -> String {
        let count = self
        
        if count == 0 {
            return "\(count)"
        } else if count < 1000 {
            return "\(count)"
        } else if count < 1000000 {
            let mod = count / 1000
            let bal = count % (mod * 1000)
            let rem = bal / 100
            return "\(mod)\(rem == 0 ? "" : ".\(rem)")k"
        } else {
            let mod = count / 1000000
            let bal = count % (mod * 1000000)
            let rem = bal / 100000
            return "\(mod)\(rem == 0 ? "" : ".\(rem)")M"
        }
    }
}
