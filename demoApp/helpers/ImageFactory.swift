//
//  imageFactory.swift
//  demoApp
//
//  Created by Akobong Ngonjoh on 9/23/21.
//

import Foundation
import UIKit

class ImageFactory: NSObject {
    
    static let shared = ImageFactory()
    
    let notLiked = UIImage(named: "notLiked")!
    let liked = UIImage(named: "liked")!
    let dot = UIImage(named: "dots")!
    let comments = UIImage(named: "comments")!
    let down = UIImage(named: "down")!
    let up = UIImage(named: "up")!
    let share = UIImage(named: "share")!
    
    func likedStatus(_ isLiked: Bool) -> UIImage {
        return isLiked ? liked : notLiked
    }
}
