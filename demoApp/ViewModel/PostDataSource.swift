//
//  PostDataSource.swift
//  demoApp
//
//  Created by Akobong Ngonjoh on 9/20/21.
//

import Foundation
import UIKit

class postDataSource<CELL : UITableViewCell,T> : NSObject, UITableViewDataSource {
    
    private var cellIdentifier: String!
    private var allPosts: [T]!
    var configCell: (CELL, T) -> () = {_,_ in }
    init(cellIdentifier: String, allPosts: [T], configCell: @escaping (CELL, T) -> ()) {
        self.cellIdentifier = cellIdentifier
        self.allPosts = allPosts
        self.configCell = configCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        allPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CELL
        cell.selectionStyle = .none
        let post = self.allPosts[indexPath.row]
        self.configCell(cell, post)
        return cell
    }
    
}
