//
//  PostModel.swift
//  demoApp
//
//  Created by Akobong Ngonjoh on 9/20/21.
//

import Foundation

class PostModel: NSObject {
    
    private var api: API!
    
    private(set) var pstData: Posts! {
        didSet {
            self.populatePostInfo()
        }
    }
    
    var populatePostInfo : (() -> ()) = {}
    
    override init() {
        super.init()
        self.api = API()
        getPostData()
    }
    
    func getPostData() {
        self.api.fetchPostData{(data) in
            self.pstData = data
        }
    }
}
